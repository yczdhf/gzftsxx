# 如何实现微信公众号一天多次推送文章图文消息

#### Description
通过微号帮平台工具提供的48小时信息推送功能实现，可以让微信公众号一天多次推送文章(图文)消息，推送对象为公众号指定粉丝，即48小时内互动的粉丝，不能给所有粉丝推送；通过功能可以实现公众号每天自动推送消息，推送消息不受公众号原有的群发次数限制...

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
